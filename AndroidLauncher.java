package com.finalprogect.game;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = this.getIntent().getExtras();
		ActionResolverAndroid actionResolverAndroid = new ActionResolverAndroid(this.getContext());

		double scale;
		if(bundle.getBoolean("isRain")){
			scale = 1.5;
		}
		else
			scale = 1;

		actionResolverAndroid.locationName = bundle.getString("Location");
		actionResolverAndroid.length = bundle.getInt("Long");
		actionResolverAndroid.umbrellaNumber = bundle.getInt("UmbrellaNumber");
		actionResolverAndroid.carNumber = bundle.getInt("CarNumber");
		actionResolverAndroid.level = actionResolverAndroid.length * 2 / 10000 -1;
		actionResolverAndroid.rainHarm = (float)Math.floor(bundle.getDouble("RainHurt")*scale*10) / 10;
		actionResolverAndroid.rainNumber = (int)(bundle.getInt("RainNumber")*scale);
		actionResolverAndroid.coinNum1 = (int)(bundle.getInt("OneDollar")*scale);
		actionResolverAndroid.coinNum10 = (int)(bundle.getInt("TenDollar")*scale);
		actionResolverAndroid.coinNum50 = (int)(bundle.getInt("FiftyDollar")*scale);

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new RainyGame(actionResolverAndroid), config);
	}

	@Override
	public void onBackPressed() {
	}
}

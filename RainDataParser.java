package com.finalprogect.game;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by marcia on 2017/12/25.
 */

public class RainDataParser {

    int TARGET = 4;
    String locationName[] = {"臺北", "新北", "桃園", "新竹", "臺中", "南投", "雲林", "嘉義", "臺南", "高雄", "屏東", "宜蘭", "台東"};

    HashMap<String, Float> rain;
    boolean isRainy[];
    boolean isReady;
    InputStream is;

    final String DATA_ID = "O-A0002-001";
    final String API_KEY = "CWB-A5E0A58D-E735-46D5-8E43-A19B67908B30";

    public RainDataParser(){
        isRainy = new boolean[locationName.length];
        rain = new HashMap<String, Float>();
        isReady = false;

//        for(int i = 0; i<locationName.length; i++){
//            rain.put(locationName[i], 0);
//        }

    }

    public boolean getXML(){
        try {
            URL url = new URL("http://opendata.cwb.gov.tw/opendataapi?dataid="+DATA_ID+"&authorizationkey="+API_KEY);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.out.print("run: http request error");
                return false;
            }
            is = urlConnection.getInputStream();

        }catch (Exception e) {e.printStackTrace();}
        return true;
    }

    public boolean parseXml(){
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(is));
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("location");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);

                Element locationElement = (Element) node;
                NodeList weatherList = locationElement.getElementsByTagName("weatherElement");
                Element weatherElement = (Element) weatherList.item(TARGET);
                float value = Float.parseFloat(weatherElement.getElementsByTagName("elementValue").item(0).getTextContent());

                value = value < 0 ? 0:value;
                NodeList parameterList = locationElement.getElementsByTagName("parameter");
                Element parameterElement = (Element) parameterList.item(0);
                String city = parameterElement.getElementsByTagName("parameterValue").item(0).getTextContent();

                if(rain.containsKey(city)){
                    rain.put(city, rain.get(city) + value);
                }else{
                    rain.put(city, value);
                }

            }

//            SAXParserFactory factory = SAXParserFactory.newInstance();
//            SAXParser saxParser = factory.newSAXParser();
//            DefaultHandler handler = new DefaultHandler() {
//
//                boolean elementName = false;
//                boolean elementValue = false;
//                boolean checkElement = false;
//                boolean city = false;
//                boolean location = false;
//                boolean checkLocation = false;
//                Integer tempRain = 0;
//
//                public void startElement(String uri, String localName,String qName,
//                                         Attributes attributes) throws SAXException {
//                    if (qName.equalsIgnoreCase("elementName"))
//                    {
//                        elementName = true;
//                    }
//                    if (qName.equalsIgnoreCase("elementValue") && checkElement)
//                    {
//                        elementValue = true;
//                    }
//                    if (qName.equalsIgnoreCase("parameterName"))
//                    {
//                        city = true;
//                    }
//
//                    if (qName.equalsIgnoreCase("parameterValue") && checkLocation)
//                    {
//                        location = true;
//                    }
//
//                }//end of startElement method
//                public void endElement(String uri, String localName,
//                                       String qName) throws SAXException {
//                }
//
//                public void characters(char ch[], int start, int length) throws SAXException {
//                    if (elementName) {
//                        elementName = false;
//                        if(TARGET.equalsIgnoreCase(new String(ch, start, length))){
//                            checkElement = true;
//                        }
//                    }
//                    if (elementValue) {
//                        tempRain = Integer.getInteger(new String(ch, start, length));
//                        elementValue = false;
//                        checkElement = false;
//                    }
//
//                    if(city){
//                        city = false;
//                        if(new String(ch, start, length).equalsIgnoreCase("CITY")){
//                            checkLocation = true;
//                        }
//                    }
//
//                    if(location){
//                        String locationStr = new String(ch, start, length);
//                        if(rain.containsKey(locationStr)){
//                            rain.put(locationStr, rain.get(locationStr) + tempRain);
//                        }else{
//                            rain.put(locationStr, tempRain);
//                        }
//                        checkLocation = false;
//                        location = false;
//                    }
//
//                }//end of characters
//            };//end of DefaultHandler object
//
//            saxParser.parse(is, handler);

        } catch (Exception e) {e.printStackTrace();}

        return true;
    }

    public HashMap getRain(){
        return rain;
    }

}



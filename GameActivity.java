package com.finalprogect.game;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class GameActivity extends AndroidApplication
{
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Bundle bundle = this.getIntent().getExtras();

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new RainyGame(new ActionResolverAndroid(this.getContext())), config);
    }

    @Override
    public void onBackPressed() {
    }

}

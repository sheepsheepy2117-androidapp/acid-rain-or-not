package com.finalprogect.game;

import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.finalprogect.game.LocationUtil.LocationHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;


public class MyLocationUsingHelper extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,ActivityCompat.OnRequestPermissionsResultCallback {

    TextView TextAddress;
    Location myLocation;
    double latitude;
    double longitude;
    LocationHelper locationHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamemenu);

        locationHelper = new LocationHelper(this);
        locationHelper.checkpermission();
        if(locationHelper.checkPlayServices())
        {
            locationHelper.buildGoogleApiClient();
        }
        myLocation = locationHelper.getLocation();
        if(myLocation != null)
        {
            latitude = myLocation.getLatitude();
            longitude = myLocation.getLongitude();
            getAddress();
        }
        else
        {
            Toast.makeText(this, "Couldn't get the location. Make sure location is enabled on the device", Toast.LENGTH_SHORT).show();
        }

    }
     public void getAddress()
     {
         Address locationAddress;
         locationAddress = locationHelper.getAddress(latitude, longitude);
         if(locationAddress != null)
         {
             String address = locationAddress.getAddressLine(0);
             String address1 = locationAddress.getAddressLine(1);
             String city = locationAddress.getLocality();
             String state = locationAddress.getAdminArea();
             String country = locationAddress.getCountryName();
             String postalCode = locationAddress.getPostalCode();


             String currentLocation;

             if(!TextUtils.isEmpty(address))
             {
                 currentLocation=address;

                 if (!TextUtils.isEmpty(address1))
                     currentLocation+="\n"+address1;

                 if (!TextUtils.isEmpty(city))
                 {
                     currentLocation+="\n"+city;

                     if (!TextUtils.isEmpty(postalCode))
                         currentLocation+=" - "+postalCode;
                 }
                 else
                 {
                     if (!TextUtils.isEmpty(postalCode))
                         currentLocation+="\n"+postalCode;
                 }

                 if (!TextUtils.isEmpty(state))
                     currentLocation+="\n"+state;

                 if (!TextUtils.isEmpty(country))
                     currentLocation+="\n"+country;

                 TextAddress.setText(currentLocation);
             }
             else
             {
                 Toast.makeText(this, "Something wrong", Toast.LENGTH_SHORT).show();
             }
         }
     }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        locationHelper.onActivityResult(requestCode,resultCode,data);
    }


    @Override
    protected void onResume() {
        super.onResume();
        locationHelper.checkPlayServices();
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i("Connection failed:", " ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        myLocation=locationHelper.getLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        locationHelper.connectApiClient();
    }


    // Permission check functions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // redirects to utils
        locationHelper.onRequestPermissionsResult(requestCode,permissions,grantResults);

    }
}

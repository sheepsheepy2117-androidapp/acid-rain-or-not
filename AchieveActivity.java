package com.finalprogect.game;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class AchieveActivity extends AppCompatActivity
{
    TextView car, rain, umbrella, win, manymoney, nomoney, map;
    int[] achieve = {0,0,0,0,0,0,0};
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achieve);
        findViews();
        getpassBundle();
    }
    protected void findViews()
    {
        car = (TextView)findViewById(R.id.achievecar);
        rain = (TextView)findViewById(R.id.achieverain);
        umbrella = (TextView)findViewById(R.id.achieveumbrella);
        win = (TextView)findViewById(R.id.achievewin);
        manymoney = (TextView)findViewById(R.id.achievemanymoney);
        nomoney = (TextView)findViewById(R.id.achievenomoney);
        map = (TextView)findViewById(R.id.achievemap);
        setInvisable();
    }
    protected void setInvisable()
    {
        car.setText("1. ???");
        rain.setText("2. ???");
        umbrella.setText("3. ???");
        manymoney.setText("4. ???");
        nomoney.setText("5. ???");
        win.setText("6. ???");
        map.setText("7. ???");
    }
    protected void getpassBundle()
    {
        Bundle bundle =this.getIntent().getExtras();
        achieve = bundle.getIntArray("achieve");
        if(achieve[0] >= 500)
            car.setText("1. 撞不死的小強!!  被車子撞並且過關500次!");
        if(achieve[1] >= 5000)
            rain.setText("2. 沒死即禿!!   被5000滴雨滴到沒死!");
        if(achieve[2] >= 1000)
            umbrella.setText("3. 雨傘救我!!   撿到1000次雨傘!");
        if(achieve[3] == 1)
            manymoney.setText("4. 過關高手!!    過關100次!");
        if(achieve[4] == 1)
            nomoney.setText("5. 窮到睡路邊了!!  錢少到不到10元!");
        if(achieve[5] >= 100)
            win.setText("6. 大財主!!   達到30000塊!");
        if(achieve[6] >= 12)
            map.setText("7. 環遊台灣!!    玩過臺灣地圖的所有關卡!");
    }
    public void ReturnMain(View v)
    {
        AchieveActivity.this.finish();
    }
}
